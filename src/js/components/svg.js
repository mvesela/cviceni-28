/**
 * čára:
 *  - nakreslit   ( drag'n'drop )
 *  - smazat      ( left click )
 *  - radial menu ( right click)
 *    - změnit barvu
 *    - smazaní
 *    - textový komentář
 * textové pole:
 *  - vytvořit    ( right click on svg )
 *    - nic => smaže se
 *    - málo znaků => zčervená
 *    - přes řádek textu => zvětší se
 *  - změnit      ( click on and write )
 *  - smazat      ( right click on textarea )
 *  - přenést     ( drag'n'drop )
 *  - radial menu ( right click on mark )
 *    - změnit barvu
 *    - smazaní
 *    - textový komentář
 * značka:
 *  - nakreslit   ( left click on svg )
 *  - přenést     ( drag'n'drop )
 *  - smazat      ( left click on mark )
 *  - radial menu ( right click)
 *    - změnit barvu
 *    - smazaní
 *    - textový komentář
 * komiksová bublina:  ( je již vytvořena předem )
 *  - napsat dovnitř
 *    - nic => zůstane
 *    - přes řádek textu => zvětší se
 */
/* eslint-disable brace-style */
import Component from './component';
import { __dispatchEvent, __removeClass, __addClass } from '../lib/utils';
import SVGDraw from './svg-lib/svg-draw';
import SVGText from './svg-lib/svg-text';

export default class SVG extends Component {
  constructor($element) {
    super($element);
    this.$canvas = this.target.querySelector('.svg-view');

    this.format = d3.format('.0f');

    this._setSize();

    this.init();
    this._initWhenDone();
  }

  init() {
    if (HISTORYLAB.import.done) return;

    // duplicate source of different svg element
    this.toDuplicate = this.target.getAttribute('data-duplicate-svg');

    // make array from data-attributes (contains what kind of actions can user do on this svg)
    const svgSpecificActions = this.target.getAttribute('data-svg-target').split(' ');

    // get default color
    const colors = this.target.getAttribute('data-svg-colors');

    for (let i = 0; i < svgSpecificActions.length; i += 1) {
      if (svgSpecificActions[i] === 'kresleni') {
        const svgSpecificColor = colors ? colors.split(' ')[0] : 'yellow';
        this._initPath(this.showFeedback, svgSpecificColor);
      }
      else if (svgSpecificActions[i] === 'znacky') {
        const svgSpecificColor = colors ? colors.split(' ')[0] : 'blue';
        this._initCircle(this.showFeedback, svgSpecificColor);
      }
      // else if (svgSpecificActions[i] === 'text') {
      //   this._initText();
      // }
    }

    // this._initZoom();
    // comic bubbles if exists
    this._initComicBubbles();

    // listen events
    this._getEvents();
    this._getDropEvent();
  }

  // eslint-disable-next-line class-methods-use-this
  _initWhenDone() {
    if (!HISTORYLAB.import.done) return;

    d3.selectAll('[data-svg-text] textarea')
      // local context needed because of `this`
      // eslint-disable-next-line func-names
      .on('mouseenter', function () {
        d3.select(this.closest('foreignObject')).raise();
      });
  }

  _setSize() {
    // NOTE: Cannot set `width` because of change of width when on half slide.
    // TODO: set size on window resize

    // might not be needed when inside sortable or other higher level module
    // TODO: add condition for other higher level modules
    // if (this.target.closest('[data-sortable-target')) return false;

    const viewbox = this.target.getAttribute('viewBox').split(' ');
    const rect = this.target.getBoundingClientRect(); // max-height: 100vh
    // const getMaxWidthAbsolute = () => {
    //   if (matchMedia('screen and (min-width: 1025px)').matches) {
    //     return window.innerWidth * .8; // max-width: 62vw
    //   }

    //   return window.innerWidth;
    // };
    const maxWidthAbsolute = window.innerWidth;
    const maxHeightAbsolute = window.innerHeight * .9;
    const getHeight = () => {
      if (!rect.height) return viewbox[3];
      if (rect.height > viewbox[3]) return viewbox[3];

      return rect.height;
    };

    const height = getHeight() > maxHeightAbsolute ? maxHeightAbsolute : getHeight();

    const scale = height / viewbox[3];
    const maxWidth = viewbox[2] * scale;
    const setMaxWidth = scale ? `${maxWidth < maxWidthAbsolute ? maxWidth : maxWidthAbsolute}px` : '';

    this.target.style.maxWidth = setMaxWidth;

    // fix for svg + draggable/gallery (horizontal layout) in Chrome
    if (
      this.target.getAttribute('data-svg-drop')
      && !this.target.closest('.layout-vertical')
    ) {
      this.target.closest('.svgs-container').style.maxWidth = setMaxWidth;
    }
  }

  // _initZoom() {
  //   const view = d3.select(`#${this.target.id} .svg-view`);
  //   const size = [
  //     view.attr('width'),
  //     view.attr('height'),
  //   ];
  //   SVGZoom.zoom(this.target, view, size);
  // }

  _initPath(callback, color) {
    d3.select(this.target)
      .call(d3.drag()
        .container(() => this.target)
        .subject(() => {
          const p = [d3.event.x.toFixed(), d3.event.y.toFixed()];

          return [p, p];
        })
        .on('start', () => {
          SVGDraw.renderPath(this.target, color, callback);
        }));
  }
  // _initText() {
  //   d3.select(this.target).on('click', () => {
  //     SVGText.openTextDialog(this.target);
  //     d3.event.preventDefault();
  //   });
  // }

  _initCircle(callback, color) {
    d3.select(this.target).on('click', () => {
      const item = d3.event.target;
      const nodeName = item.nodeName;
      const validPlace = (nodeName === 'image');
      // const isCircle = (__hasClass(item, 'svg-circle'));

      if (!validPlace) {
        return;
      }

      SVGDraw.createCircle(this.target, color);
      callback();

      d3.event.preventDefault();
    }, false);
  }

  _initComicBubbles() {
    const $comicBubbles = this.target.querySelectorAll('.svg-komiks .svg-textarea');

    if ($comicBubbles) {
      $comicBubbles.forEach(($comicBubble) => {
        SVGText.comicBubblesEventsManager(this.target, $comicBubble);
      });
    }
  }

  _generateNewID(oldID) {
    const idParts = oldID.split('_');
    // beware if it's a error, return oldID
    if (idParts.length < 2) {
      console.log('Svg ID error!', oldID);
      return oldID;
    }

    // solve thirt part of id
    if (idParts[idParts.length - 1] !== `clone-${idParts[0]}`) {
      idParts.push(`clone-${idParts[0]}`);
    }

    // solve first part of id
    idParts[0] = this.target.id;

    const newID = idParts.join('_');
    return newID;
  }

  _getEvents() {

    const svgDuplicateSources = this.toDuplicate ? this.toDuplicate.split(' ') : [];

    if (svgDuplicateSources[0]) {

      document.addEventListener('svg.change', (e) => {
        if (svgDuplicateSources.includes(e.detail.svg.id)) {
          const $templateNode = e.detail.svg.node;
          const nodeName = e.detail.svg.nodeName;
          const localNodeID = this._generateNewID($templateNode.id);
          let $localNode = this.target.querySelector(`#${localNodeID}`);
          let task;
          // for creating new element or we edit the old one after created one
          if ($localNode || e.detail.svg.task === 'create') {
            task = e.detail.svg.task;
          } else {
            return;
          }

          /**
           * 1. divided by node name
           * 2. divided by task type
           * path => svg lines
           * TEXTAREA => text fields or comic bubbles
           * foreignObject => svg circles
           */
          if (nodeName === 'path') {
            switch (task) {
              case 'create':
                this._clonePath($templateNode, localNodeID);
                break;
              case 'color':
                __removeClass($localNode, 'red blue');
                __addClass($localNode, e.detail.svg.color);
                break;
              case 'remove':
                $localNode.remove();
                break;
              default:
                console.log('Nondefinated path events task. :(');
            }
          } else if (nodeName === 'TEXTAREA') {
            if (e.detail.svg.isBubble) {
              switch (task) {
                case 'create':
                  this._cloneComicBubbles($templateNode, localNodeID);
                  break;
                case 'change':
                  if ($templateNode.value.length !== 0) {
                    $localNode.value = $templateNode.value;
                    // if needs resize
                    if ($localNode.style.height !== $templateNode.style.height) {
                      $localNode.style.height = $templateNode.style.height;
                    }
                  } else {
                    $localNode.parentElement.remove();
                  }
                  break;
                default:
                  console.log('Nondefinated comicBubble events task. :(');
              }
            } else {
              switch (task) {
                case 'create':
                  this._cloneTextField(e.detail.svg.isWrong, $templateNode, localNodeID);
                  break;
                case 'change':
                  $localNode.value = $templateNode.value;
                  // if is now wrong lenght
                  if (e.detail.svg.isWrong) {
                    __addClass($localNode, 'is-wrong');
                  } else {
                    __removeClass($localNode, 'is-wrong');
                  }
                  // if needs resize
                  if ($localNode.style.height !== $templateNode.style.height) {
                    $localNode.style.height = $templateNode.style.height;
                  }
                  break;
                case 'color':
                  $localNode = $localNode.parentElement.childNodes[0];
                  __removeClass($localNode, 'red blue');
                  __addClass($localNode, e.detail.svg.color);
                  break;
                case 'move':
                  if ($localNode) {
                    $localNode.parentElement.remove();
                  }
                  this._cloneTextField(e.detail.svg.isWrong, $templateNode, localNodeID);
                  break;
                case 'remove':
                  $localNode.parentElement.remove();
                  break;
                default:
                  console.log('Nondefinated textField events task. :(');
              }
            }
          } else if (nodeName === 'foreignObject') {
            switch (task) {
              case 'create':
                this._cloneCircle($templateNode, localNodeID);
                break;
              case 'color':
                $localNode = $localNode.childNodes[0];
                __removeClass($localNode, 'red blue');
                __addClass($localNode, e.detail.svg.color);
                break;
              case 'move':
                if ($localNode) {
                  $localNode.remove();
                }
                this._cloneCircle($templateNode, localNodeID);
                break;
              case 'remove':
                $localNode.remove();
                break;
              default:
                console.log('Nondefinated circle events task. :(');
            }
          } else {
            console.log('Nondefinated events nodeName. :(', nodeName);
          }

          // dispatch event that svg has changed
          __dispatchEvent(document, 'svg.change', {}, {
            svg: {
              id: this.target.id,
              task: e.detail.svg.task,
              node: $templateNode,
              nodeName,
              contextMenu: false,
            },
          });
        }
      });
    }
  }

  _getDropEvent() {
    // need if we drop more than one same item
    let idSalt = 0;

    document.addEventListener('svg.drop', (event) => {

      if (event.detail.svg.id === this.target.id) {

        const { $node, position, type, size } = event.detail.svg;

        const getMultiply = () => {
          // make images smaller when dropped
          if (type === 'image' || type === 'svg') return .62;
          // make tags larger when dropped to not jump on more lines
          if (type === 'tag') return 1.075;

          return 1;
        }
        const multiply = getMultiply();

        size.width = size.width * multiply;
        size.height = size.height * multiply;

        const $d3canvas = d3.select(`#${this.target.id} .svg-view`);
        const $d3foreignObject = $d3canvas.append('svg:foreignObject')
          .attr('width', size.width)
          .attr('height', size.height)
          .attr('x', this.format(position[0] - (size.width / 2)))
          .attr('y', this.format(position[1] - (size.height / 2)))
          .attr('data-svg-resolution', `${$d3canvas.attr('width')}, ${$d3canvas.attr('height')}`)
          .attr('data-svg-type', 'pretahovani')
          .attr('id', `${this.target.id}_${$node.getAttribute('data-id')}_${idSalt}_dropItem`);
          // _dropItem is important identifier

        const $dropItem = document.createElement('div');
        $dropItem.classList.add('dropped__item', `dropped__item--${type}`);
        $dropItem.appendChild($node.firstElementChild.firstElementChild);

        $d3foreignObject.append(() => $dropItem);

        const $foreignObject = $d3foreignObject.node();

        // increment salt
        idSalt += 1;

        // add events to element
        SVGDraw.circleEventsManager(this.target, $foreignObject, $node, 'pretahovani');

        // dispatch event that svg has changed
        __dispatchEvent(document, 'svg.change', {}, {
          svg: {
            id: this.target.id,
            task: 'create',
            node: $foreignObject,
            nodeName: $foreignObject.nodeName,
            contextMenu: false,
          },
        });
      }
    });
  }

  _clonePath($templateNode, localNodeID) {
    const $localNode = $templateNode.cloneNode(true);
    $localNode.id = localNodeID;
    this.$canvas.appendChild($localNode);
    // add events to element
    SVGDraw.pathEventsManager(this.target, $localNode);
  }

  _cloneTextField(isWrong, $templateNode, localNodeID) {
    const $localNode = $templateNode.parentElement.cloneNode(true);
    $localNode.childNodes[1].id = localNodeID;
    this.$canvas.appendChild($localNode);
    if (isWrong) {
      __addClass($localNode, 'is-wrong');
    } else {
      __removeClass($localNode, 'is-wrong');
    }
    // add events to element
    SVGText.textEventsManager(this.target, $localNode.childNodes[1], false);
  }

  _cloneCircle($templateNode, localNodeID) {
    const $localNode = $templateNode.cloneNode(true);
    $localNode.id = localNodeID;
    this.$canvas.appendChild($localNode);
    // add events to element
    const $node = $localNode.firstChild;
    SVGDraw.circleEventsManager(this.target, $localNode, $node, $templateNode.getAttribute('data-svg-type'));
  }

  _cloneComicBubbles($templateNode, localNodeID) {
    const $localNode = $templateNode.parentElement.cloneNode(true);
    $localNode.childNodes[1].id = localNodeID;
    this.$canvas.appendChild($localNode);
    // add events to element
    SVGText.comicBubblesEventsManager(this.target, $localNode.childNodes[1]);
  }

  static selectItem(item) {
    item.setAttribute('data-svg-selected', 'true');
  }

  // Remove 'selected' classname from element within SVG
  static deselectItem() {
    // previously "selected" elements
    const selected = document.querySelectorAll('[data-svg-selected="true"]');
    // unselect them
    for (let i = 0; i < selected.length; i += 1) {
      selected[i].setAttribute('data-svg-selected', 'false');
    }
  }

  // Remove element from within SVG
  // [data-svg-selected="true"] marks selected element in SVG
  static removeItem(isRadial, element = '[data-svg-selected="true"]') {
    let $element = document.querySelector(element);
    let nodeName = $element.nodeName;
    const $parent = $element.parentElement; // for <foreignObject>

    // TEXTAREA => text field(right click in textarea)
    // DIV => text field(click in radial menu) or circle
    switch (nodeName) {
      case 'path':
        // $element = $element;
        break;
      case 'TEXTAREA':
        // $element = $element;
        break;
      case 'DIV':
        if ($parent.childNodes[1]) {
          $element = $parent.childNodes[1];
        } else {
          $element = $parent;
        }
        nodeName = $element.nodeName;
        break;
      default:
        console.log('Nondefinated node type. :(', nodeName);
    }

    // get svg id from object id
    const svgID = $element.id.split('_')[0];

    // dispatch event that svg has changed
    __dispatchEvent(document, 'svg.change', {}, {
      svg: {
        id: svgID,
        task: 'remove',
        node: $element,
        nodeName,
        contextMenu: isRadial,
      },
    });

    if ($parent.nodeName === 'foreignObject') {
      d3.select($parent).remove();

      // dispatch event that drop item was deleted
      const idParts = $parent.id.split('_');
      if (idParts[idParts.length - 1] === 'dropItem') {
        __dispatchEvent(document, 'svg.drop.delete', {}, {
          item: {
            id: idParts[1],
            $object: $element.firstChild,
          },
        });
      }
    } else {
      d3.select($element).remove();
    }
  }

  // Unmark all SVGs with 'svg-active' classname
  static deactivate() {
    const allActiveSvgs = document.querySelectorAll('[data-svg-active="true"]');
    for (let i = 0; i < allActiveSvgs.length; i += 1) {
      allActiveSvgs[i].setAttribute('data-svg-active', 'false');
    }
  }

  // Mark active SVG with 'svg-active' classname
  static activate(svg) {
    SVG.deactivate();
    svg.setAttribute('data-svg-active', 'true');
  }

  // Remove connection between clone and template
  // Remove third part of id if exists
  static removeCloneConnectionManager($node) {
    const idParts = $node.id.split('_');
    if (idParts[idParts.length - 1].includes('clone')) {
      idParts.pop();
      $node.id = idParts.join('_');
      return true;
    }
    return false;
  }
}
